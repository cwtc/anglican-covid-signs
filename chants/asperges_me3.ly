\include "../common_liturgy.ly"

\paper {
	check-consistency = ##f
}

\layout {
  ragged-right = ##f
}

Introit = \relative c' {
	\clef treble
	\key e \minor
	\override Staff.TimeSignature #'stencil = ##f 
	\override ParenthesesItem.font-size = #2
	\hide Staff.Stem
	\cadenzaOn
	% \mark \markup{ \fontsize #-2 { \italic Antiphon }}
	% \ph
	% d4( e4) g4( fis4 e4) fis4( g4) a2
	% b4( c4 d2 c4) b4( a2)
	% b4 a4( g4) fis4( g4) a4( g4 e4) fis4 d4 d4( e4 d4) d2 \bar "|"
	% % \mark \markup{ \fontsize #-2 { \caps Psalm 51}}

	% \ph 
	% d4( e4) g4( fis4 e4) fis4( g4) a2
	% a4( b4 c4 b4 a4) g4( fis4) e4 g4 fis4( g4) a4( g4 e4 fis4)
	% d4( e4 d4) d2 \bar "||"

	% \mark \markup{ \fontsize #-2 { \italic Verse }}
	% \ph
	% d4( g4 fis4) g4( a4) a4 a4 a4( c4) b4 b4( a4) a2( b2) \bar "|"
	% \ph
	% % \endOfLine \mark \markup{ \fontsize #-2 { \caps Psalm 51}}
	% a4( fis4) g4( a4) a4( b4 c4) a4 g2 e2( d2) \bar "||"
	% % \endOfLine \mark \markup{ \fontsize #-2 { \caps Psalm 51}}
	% % \break

	\mark \markup{ \fontsize #-2 \italic {Gloria Patri}}
	\ph
	d4( g4 fis4) g4( a4) a1 
	a4 a4( c4) b4 b4 b4( a4) a2( b2) \bar "||"
	\ph
	a4( fis4) g4( a4) a1 \bar "|"
	a4 a4( c4) b4 b4( a4) a2( b2) \bar "|"
	a4( fis4) g4( a4) a4( b4 c4) a2
	g2 e2( d2) \bar "||"


	\cadenzaOff
}

LyricsOne = \lyricmode{
	\override LyricText.self-alignment-X = #LEFT
	% \normal
	% \Vbar Thou __ shalt __ purge __ me,
	% O __ Lord, __
	% with hys -- sop, and __ I shall be __ clean:

	% % \bold
	% \Rbar Thou __ shalt __ wash __ me,
	% and __ I __ shall be whi -- ter __
	% than __ snow.

	% % \normal
	% \Vbar Have __ mer -- cy up -- on __ me, O __ God: __
	% % \bold
	% \Rbar af -- ter __ thy __ great good -- ness. __

	% \normal
	\Vbar Glo -- ry __ "be to the Father, and to the Son,"
	and to __ the Ho -- ly __ Ghost: __
	% \bold
	\Rbar As __ it __ "was in the beginning, is now,"
	and ev -- er shall __ be: __
	world __ with -- out __ end.
	A -- men. __
	
}

\score {
	\new Staff <<
	\new Voice = "One" {\Introit}
	\new Lyrics \lyricsto "One" {\LyricsOne}
	>>
}

