'''
Windows installation script
'''

import os
import shutil
import subprocess
import urllib.request
import zipfile

if __name__ == '__main__':
    # Set up temp dir
    if not os.path.isdir('install_temp'):
        os.makedirs('install_temp')

    # Install TinyTeX
    appdata_dir = os.getenv('APPDATA')
    tinytex_dir = os.path.join(appdata_dir, 'TinyTeX')
    if not os.path.isdir(tinytex_dir):
        print('Installing TinyTeX...')
        tinytex_script = os.path.join('install_temp', 'install-windows.bat')
        urllib.request.urlretrieve('https://yihui.org/gh/tinytex/tools/install-windows.bat',
                                   tinytex_script)
        tinytex_install = subprocess.Popen([tinytex_script], shell=True)
        tinytex_install.communicate()

        # Install necessary packages
        print('Updating tlmgr (LaTeX package manager)...')
        tex_update = subprocess.Popen(['tlmgr', 'update', '--self'], shell=True)
        tex_update.communicate()
        print('Installing TinyTeX packages...')
        packages = ['paracol', 'makecell', 'xtab', 'draftwatermark', 'everypage', 'tocloft',
                    'titlesec', 'lipsum', 'titling']
        for package in packages:
            print('\t Installing {0}...'.format(package))
            package_install = subprocess.Popen(['tlmgr', 'install', package], shell=True)
            package_install.communicate()

    else:
        print('TinyTeX already installed.')

    # Install Sublime Text
    pf_dir = os.environ['ProgramFiles']
    sublime_dir = os.path.join(pf_dir, 'Sublime Text 3')
    if not os.path.isdir(sublime_dir):
        print('Installing Sublime Text 3...')
        sublime_script = os.path.join('install_temp', 'install-sublime.exe')
        urllib.request.urlretrieve('https://download.sublimetext.com/Sublime%20Text%20Build%203211%20Setup.exe',
                                   sublime_script)
        sublime_install = subprocess.Popen([sublime_script], shell=True)
        sublime_install.communicate()
    else:
        print('Sublime Text 3 already installed.')

    # Install Package Control
    # print('Installing Package Control for Sublime Text 3...')
    # pc_dir = os.path.join(sublime_dir, 'Installed Packages')
    # pc_path = os.path.join(pc_dir, 'Package Control.sublime-package')
    # urllib.request.urlretrieve('https://packagecontrol.io/Package%20Control.sublime-package',
    #                            pc_path)

    # Install LaTeXTools
    print('Installing LaTeXTools for Sublime Text 3...')
    p_dir = os.path.join(sublime_dir, 'Packages')
    if not os.path.isdir(p_dir):
        os.makedirs(p_dir)
    urllib.request.urlretrieve('https://github.com/SublimeText/LaTeXTools/archive/master.zip',
                               os.path.join(p_dir, 'LaTeXTools.zip'))
    with zipfile.ZipFile(os.path.join(p_dir, 'LaTeXTools.zip'), 'r') as zf:
        zf.extractall(os.path.join(p_dir, 'LaTeXTools'))

    # Install LilyPond
    pf86_dir = os.environ['ProgramFiles(x86)']
    lilypond_dir = os.path.join(pf86_dir, 'LilyPond')
    if not os.path.isdir(lilypond_dir):
        print('Installing LilyPond...')
        lilypond_script = ('install_temp', 'lilypond-2.20.0-1.mingw.exe')
        urllib.request.urlretrieve('https://lilypond.org/download/binaries/mingw/lilypond-2.20.0-1.mingw.exe',
                                   lilypond_script)
        lilypond_install = subprocess.Popen([lilypond_script], shell=True)
    else:
        print('LilyPond already installed.')

    # Clean up
    shutil.rmtree('install_temp')

    print('Installation done.')
