#!/usr/bin/env bash

# Unix installation script

# Set up temp dir
mkdir -p "install_temp"

# 32 or 64 bit?
BIT=`arch`

# Install TinyTex
if [[ -d  "$HOME/.TinyTeX" ]]; then
    echo "TinyTex already installed."
else
    echo "Installing TinyTex..."
    wget -P install_temp "https://yihui.org/gh/tinytex/tools/install-unx.sh"
    sh install_temp/install-unx.sh
    # Install necessary packages
    echo "Installing necessary packages..."
    for pkg in paracol makecell xtab draftwatermark everypage tocloft titlesec lipsum titling; do
	tlmgr install $pkg
    done
fi


# Install Sublime Text
if [[ $(dpkg-query -W -f='${Status}' sublime-text 2>/dev/null | grep -c "ok installed") ]]; then
    echo "Sublime Text already installed."
else
    echo "Installing Sublime Text..."
    # Install key
    wget -qO- "https://download.sublimetext.com/sublimehq-pub.gpg" | sudo apt-key add -
    # Add apt repo
    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
    # Install
    sudo apt-get update
    sudo apt-get install sublime-text
fi


# Install LaTeXTools
echo "Installing LaTeXTools for Sublime Text..."
mkdir -p "/opt/sublime_text/Packages"
sudo wget -P "/opt/sublime_text/Packages" "https://github.com/SublimeText/LaTeXTools/archive/master.zip"
sudo unzip -d "/opt/sublime_text/Packages/LaTeXTools" "/opt/sublime_text/Packages/master.zip"

# Install LilyPond
if [[ -d "$HOME/Applications/LilyPond.app" ]]; then
    echo "LilyPond already installed."
else
    if [[ $BIT == "x86_64" ]]; then
        # 64 bit
        wget -P install_temp "https://lilypond.org/download/binaries/linux-64/lilypond-2.20.0-1.linux-64.sh"
        sh install_temp/lilypond-2.20.0-1.linux-64.sh
    else
        # 32 bit
        wget -P install_temp "https://lilypond.org/download/binaries/linux-x86/lilypond-2.20.0-1.linux-x86.sh"
        sh install_temp/lilypond-2.20.0-1.linux-x86.sh
    fi
fi

# Clean up
rm -rf install_temp

echo "Installation done."
