#!/usr/bin/env bash

rm -rf "./install_temp"

rm -rf "$HOME/Library/TinyTeX"

rm -rf "$HOME/Applications/Sublime Text.app"

rm -rf "$HOME/Applications/LilyPond.app"
