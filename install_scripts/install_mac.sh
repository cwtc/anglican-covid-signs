#!/usr/bin/env bash

# macOS installation script

# Set up temp dir
mkdir -p "install_temp"

# Create home Applications dir if it doesn't exist
if ! [[ -d "$HOME/Applications" ]]; then
    mkdir "$HOME/Applications"
fi

# Install TinyTex
if [[ -d "$HOME/Library/TinyTeX" ]]; then
    echo "TinyTex already installed."
else
    echo "Installing TinyTex..."
    wget -P install_temp "https://yihui.org/gh/tinytex/tools/install-unx.sh"
    sh install_temp/install-unx.sh
    # Install necessary packages
    echo "Installing necessary packages..."
    for pkg in paracol makecell xtab draftwatermark everypage tocloft titlesec lipsum titling; do
	tlmgr install $pkg
    done
fi


# Install Sublime Text
if [[ -d "$HOME/Applications/Sublime Text.app" ]]; then
    echo "Sublime Text already installed."
else
    echo "Installing Sublime Text..."
    # TODO: install_sublime.dmg not being written to install_temp dir...
    wget -O "install_temp/install_sublime.dmg" "https://download.sublimetext.com/Sublime%20Text%20Build%203211.dmg"
    MOUNTDIR=$(hdiutil mount "install_temp/install_sublime.dmg" | tail -1 | awk '{$1=$2=""; print $0}' | xargs)
    cp -r "$MOUNTDIR/Sublime Text.app" "$HOME/Applications/"
    hdiutil unmount "$MOUNTDIR"
fi


# Install LaTeXTools
echo "Installing LaTeXTools for Sublime Text..."
mkdir -p "$HOME/Applications/Sublime Text.app/Packages"
wget -P "$HOME/Applications/Sublime Text.app/Packages" "https://github.com/SublimeText/LaTeXTools/archive/master.zip"
unzip -d "$HOME/Applications/Sublime Text.app/Packages/LaTeXTools" "$HOME/Applications/Sublime Text.app/Packages/master.zip"

# Install LilyPond
if [[ -d "$HOME/Applications/LilyPond.app" ]]; then
    echo "LilyPond already installed."
else
    # macOS version matters! At 10.15+, 32 bit is no longer supported
    base_ver=10.14
    ver=$(sw_vers | grep ProductVersion | cut -d':' -f2 | tr -d ' ')
    if [[ $(echo -e "$base_ver\n$ver" | sort -V | tail -1) == "$base_ver" ]]; then
	wget -P "install_temp" "https://bintray.com/marnen/lilypond-darwin-64/download_file?file_path=lilypond-2.20.0.build20200311175017-darwin-64.tar.gz"
	tar -xf "install_temp/lilypond-2.20.0.build20200311175017-darwin-64.tar.gz" -C "$HOME/Applications/"
    else
	wget -P "install_temp" "https://lilypond.org/download/binaries/darwin-x86/lilypond-2.20.0-1.darwin-x86.tar.bz2"
	tar -xjf "install_temp/lilypond-2.20.0-1.darwin-x86.tar.bz2" -C "$HOME/Applications/"
    fi
fi

echo "Installation done."
