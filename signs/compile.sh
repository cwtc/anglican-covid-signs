#!/usr/bin/env bash

# Compile music snippets with LilyPond into an output dir.
if [ $# -gt  1 ]; then
	if [ "$2" == "debug" ]; then
		# If we are debugging, avoid rewriting into out/.
		# The files should already be in the top level dir.
        echo "Compiling LilyPond in debug mode."
		lilypond-book --pdf "$1.lytex"
	else
		echo "If a second argument is provided, it should be 'debug'."
        echo "Your provided second argument: "
        echo $2
		exit 1
	fi
else
	mkdir -p out
    echo "Compiling LilyPond."
	lilypond-book --output=out --pdf "$1.lytex" 	
fi

# For paths in the lytex file to not break, we need to move the output
# back into the original directory.
outfiles=()
for file in out/*; do
    name=$(basename $file)
    # echo $name
    outfiles+=($(basename $file))
	mv $file ../
done
# echo $outfiles

# We can now safely compile the latex without breaking the paths.
xelatex $1

# If we are actively developing, compilation will take a long time
# if we delete everything every time. So only finish the script
# if we want to clean up.
if [ $# -gt  1 ]; then
    if [ "$2" == "debug" ]; then
		echo "Not cleaning up, in debug mode."
	else
		echo "If a second argument is provided, it should be 'debug'."
        echo "Your provided second argument: "
        echo $2
		exit 1
	fi
else
    echo "Cleaning up..."
	for file in $outfiles; do
		if [ -d $file ]; then
			rm -r $file
		else
			rm $file
		fi
	done	
fi

