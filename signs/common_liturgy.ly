% This file contains Lilypond elements common to all
% snippets of music we need to generate.

% Set up the fonts and staff size.
% Note that by convention, the three fonts, in order, are
% Roman, Sans, and Typewriter (so when we need to use Junicode,
% for example, we have to call \sans.)
% The particularities of Lilypond!
\paper {
	#(define fonts
		(make-pango-font-tree "Sabon LT Std"
							  "Junicode"
                      		  "Sabon LT Std"
        	(/ staff-height pt 20)
        )
     )
}

% Define versicle and response
Vbar = \markup{ \fontsize #1.5 \sans "℣   " }
#(define-markup-command (versicle layout props text) (markup?)
  (interpret-markup layout props
    #{\markup \concat { \Vbar #text } #} ))

Rbar = \markup{ \fontsize #1 \sans "℟  " }  % Different sizes, but looks right
#(define-markup-command (response layout props text) (markup?)
  (interpret-markup layout props
    #{\markup \concat { \Rbar #text } #} ))

% Define "placeholder" note so V/R symbols can be placed to the
% left of the real notes
ph = \relative c'' { \once \hide NoteHead \once \hide Stem g4 }

% Define large and small sign of the cross
cross = \markup{ \typewriter "✠ "}
scross = \markup{ \typewriter "✛"}